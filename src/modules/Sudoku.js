import { Box } from './Box';
import { Algorithms } from './Algorithms';

export class Sudoku extends Algorithms {
    constructor(sudoku, options) {
        super(options);

        this.inputSudoku = this.generateSudoku(sudoku);
        this.sudoku = this.generateSudoku(sudoku);
        this.lastCorrectResult = this.generateSudoku(sudoku);

        this.options = {
            can: [1, 2, 3, 4, 5, 6, 7, 8, 9],
            empty: 0,
            w: 3,
            h: 3,
            ...options
        };

        this.check();
    }

    // Core: Main check init.
    check() {
        let isNewCanNums = false;

        this.sudoku.map(rowArr => {
            return rowArr.map(box => {
                if (box.isEmpty) {
                    const lastResultBox = this.lastCorrectResult[box.row][box.col];         // get lastResult box.
                    box.sudoku = lastResultBox.sudoku = this.getSudokuNums(this.sudoku);    // set new sudoku nums for correct calculation.
                    const newCanNums = this.checkAll(box, this.options.can);                // check new canNums.
                    newCanNums.length !== box.canNums.length && (isNewCanNums = true);      // check new changes in canNums and lastCanNums lengths.
                    box.canNums = lastResultBox.canNums = newCanNums;                       // set new box canNums.
                }
                return box;
            });
        });

        if (isNewCanNums) {
            this.check();

        } else if (!this.isFill(this.sudoku)) {
            const lastCorrectArray = this.lastCorrectResult.reduce((acc, row) => {
                return acc.concat(row).sort((a, b) => a.canNums.length - b.canNums.length);
            }, []).filter(box => box.isEmpty);

            console.log(`
                Progress: ${this.getProgress.percentage}% [${this.getProgress.inputEmpty - this.getProgress.outputEmpty}/${this.getProgress.inputEmpty}].
                Need to calc with more algorithms or by selection method.
                Last correct result of empty boxed sorted by canNums length:
            `, lastCorrectArray);
        } else {
            console.log(`
                Solved.
                Progress: ${this.getProgress.percentage}% [${this.getProgress.inputEmpty - this.getProgress.outputEmpty}/${this.getProgress.inputEmpty}].
            `);
        }
    }

    // Controller: Get array of keys that can be in current box.
    checkAll(box, canNums) {
        const canBox = this._canInArea(box.fullAreaNums, canNums);
        const canRow = this._canInRow(box.fullRowNums, canBox);
        const canCol = this._canInCol(box.fullColNums, canRow);

        // Check exceptions.
        const canRowExcept = this._canRowExcept(box, canCol);
        const canColExcept = this._canColExcept(box, canRowExcept);
        const canAreaExcept = this._canAreaExcept(box, canColExcept);

        return canAreaExcept;
    }

    // Util: Generate structure of sudoku.
    generateSudoku(sudoku) {
        const sudokuCopy = this.copySudoku(sudoku);
        return sudokuCopy.map((rArr, r) => rArr.map((val, c) => new Box(sudokuCopy, r, c, this.options)));
    }

    // Util: Check that previous sudoku equals to current.
    isEqual(oldSudoku, currentSudoku) {
        return JSON.stringify(this.getSudokuNums(oldSudoku)) === JSON.stringify(this.getSudokuNums(currentSudoku));
    }

    // Util: Check that sudoku is fill.
    isFill(sudoku) {
        return !JSON.stringify(this.getValues(sudoku)).includes(this.options.empty);
    }

    // Util: Copy sudoku.
    copySudoku(sudoku) {
        return JSON.parse(JSON.stringify(sudoku));
    }

    // Util: Get progress percentage.
    get getProgress() {
        const inputEmpty = this.getBoxes(this.inputSudoku).filter(v => v.isEmpty).length;
        const outputEmpty = this.getBoxes(this.sudoku).filter(v => v.isEmpty).length;
        const percentage = +((100 - outputEmpty / inputEmpty * 100).toFixed(2));
        return { inputEmpty, outputEmpty, percentage };
    }
}