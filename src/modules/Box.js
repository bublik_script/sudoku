export class Box {
    constructor(sudoku, row, col, options) {
        this.row = row;
        this.col = col;
        this.sudoku = sudoku;
        this.options = {
            can: [1, 2, 3, 4, 5, 6, 7, 8, 9],
            empty: 0,
            w: 3,
            h: 3,
            ...options
        };
        this.canNums = this.options.can;
    }

    // Core: Value of box.
    get value() {
        return this.canNums.length === 1 && this.sudoku[this.row][this.col] === this.options.empty
            ? this.canNums[0]
            : this.sudoku[this.row][this.col];
    }

    // Core: Return full row.
    get fullRow() {
        return this.sudoku[this.row].map((key, c) => new Box(this.sudoku, this.row, c));
    }

    // Core: Return full row of nums.
    get fullRowNums() {
        return this.sudoku[this.row];
    }

    // Core: Return full col.
    get fullCol() {
        return this.sudoku.map((row, r) => new Box(this.sudoku, r, this.col));
    }

    // Core: Return full col of nums.
    get fullColNums() {
        return this.sudoku.map(row => row[this.col]);
    }

    // Core: Return 3x3 array of current area.
    get area() {
        const {row, col} = this.areaStartRowCol;
        const area = [];
        for (let r = row; r < row + this.options.h; r++) {
            area.push(
                this.sudoku[r]
                    .slice(col, col + this.options.w)
                    .map((key, c) => new Box(this.sudoku, r, col + c))
            );
        }
        return area;
    }

    // Core: Return 3x3 array nums of current area.
    get areaNums() {
        const {row, col} = this.areaStartRowCol;
        const area = [];
        for (let r = row; r < row + this.options.h; r++) {
            area.push(this.sudoku[r].slice(col, this.options.w));
        }
        return area;
    }

    // Core: Return one line array of current area.
    get fullArea() {
        const {row, col} = this.areaStartRowCol;
        const fullArea = [];
        for (let r = row; r < row + this.options.h; r++) {
            for (let c = col; c < col + this.options.w; c++) {
                fullArea.push(new Box(this.sudoku, r, c));
            }
        }
        return fullArea;
    }

    // Core: Return one line array of current area.
    get fullAreaNums() {
        const {row, col} = this.areaStartRowCol;
        const fullArea = [];
        for (let r = row; r < row + this.options.h; r++) {
            for (let c = col; c < col + this.options.w; c++) {
                fullArea.push(this.sudoku[r][c]);
            }
        }
        return fullArea;
    }

    // Util: Get row in area.
    get rowInArea() {
        return this.row % this.options.h;
    }

    // Util: Get col in area.
    get colInArea() {
        return this.col % this.options.w;
    }

    // Util: Return start row and start col of current area.
    get areaStartRowCol() {
        const row = Math.floor(this.row / this.options.h) * this.options.h;
        const col = Math.floor(this.col / this.options.w) * this.options.w;
        return {row, col};
    }

    // Util: Is empty box.
    get isEmpty() {
        return this.value === this.options.empty;
    }

    // Util: Is area fill.
    get isAreaFill() {
        return !this.fullArea.map(({ key }) => key).includes(this.options.empty);
    }
}