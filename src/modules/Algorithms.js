import { Utils } from './Utils';

// https://habr.com/post/173795

export class Algorithms extends Utils {
    constructor(options) {
        super();

        this.options = {
            can: [1, 2, 3, 4, 5, 6, 7, 8, 9],
            empty: 0,
            w: 3,
            h: 3,
            ...options
        };
    }

    // Return array of numbers which not exist in area.
    _canInArea(areaNums, canNums) {
        return this.canInLine(areaNums, canNums);
    }

    // Return array of numbers which not exist in row.
    _canInRow(rowNums, canNums) {
        return this.canInLine(rowNums, canNums);
    }

    // Return array of numbers which not exist in col.
    _canInCol(colNums, canNums) {
        return this.canInLine(colNums, canNums);
    }

    // Get array of keys that can be only in current box in full row.
    _canRowExcept(currBox, canNums) {
        const lines = currBox.fullRow.reduce((acc, box) => {
            (box.isEmpty && box.col !== currBox.col) && acc.push(currBox.sudoku.map(r => r[box.col]));
            return acc;
        }, []);
        const canOnly = this.cantInEveryLine(lines, canNums);
        return canOnly.length ? canOnly : canNums;
    }

    // Get array of keys that can be only in current box in full column.
    _canColExcept(currBox, canNums) {
        const lines = currBox.fullCol.reduce((acc, box) => {
            (box.isEmpty && box.row !== currBox.row) && acc.push(currBox.sudoku[box.row]);
            return acc;
        }, []);
        const canOnly = this.cantInEveryLine(lines, canNums);
        return canOnly.length ? canOnly : canNums;
    }

    // Get array of keys that can be only in current box in full area.
    _canAreaExcept(box, canNums) {
        const { row, col } = box.areaStartRowCol;
        const lines = [];
        for (let r = row; r < row + this.options.h; r++) {
            if (r !== box.row && [...Array(3)].some((a, i) => box.sudoku[r][col + i] === this.options.empty)) {
                lines.push(box.sudoku[r]);
            }
        }
        for (let c = col; c < col + this.options.w; c++) {
            if (c !== box.col && [...Array(3)].some((a, i) => box.sudoku[row + i][c] === this.options.empty)) {
                lines.push(box.sudoku.map(r => r[c]));
            }
        }
        const canOnly = this.cantInEveryLine(lines, canNums);
        return canOnly.length ? canOnly : canNums;
    }
}