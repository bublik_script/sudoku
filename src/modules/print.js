export function printSudoku(sudoku, id = 'root') {
    const container = document.getElementById(id);
    let str = '';
    str += '<table>';
    sudoku.map(row => {
        str += '<tr>';
        row.map(box => {
            str += '<td><b>';
            str += !box.isEmpty ? box.value : '';
            str += '</b></td>';
        });
        str += '</tr>';
    });
    str += '</table>';
    container.innerHTML += str;
}