export class Utils {

    // Util: Return array of keys that not exist in array line.
    canInLine(line, canNums) {
        return canNums.filter(key => !line.includes(key));
    }

    // Util: Return array of keys that can't be in every line of array of lines.
    cantInEveryLine(lines, canNums) {
        return canNums.filter(key => lines.every(line => line.includes(key)));
    }

    // Util: Get all values in matrix structure.
    getSudokuNums(sudoku) {
        return sudoku.map(rArr => rArr.map(box => box.value));
    }

    // Util: Get all boxes of sudoku in inline array.
    getBoxes(sudoku) {
        return sudoku.reduce((acc, r) => acc.concat(r), []);
    }

    // Util: Get all values of sudoku in inline array.
    getValues(sudoku) {
        return this.getSudokuNums(sudoku).reduce((acc, r) => acc.concat(r), []);
    }
}
