# Sudoku solving algorithm

## Getting Started

Clone this repository in your folder by using commands:

```
cd <your_folder>
git clone <repo_link> .
```

## Install dependencies

Before start the project you need to install all dependencies:

```
npm i
```
or
```
yarn install
```

## Start project in development mode

```
npm start
```
or
```
yarn start
```

## Deployment

To build the project in production you should use next command:

```
npm run build
```
or
```
yarn build
```

It's generating 'dist' folder in root with indexes files.

## Running the tests

No tests.

## Authors

[Bublik](https://bitbucket.org/bublik_script)

## License

This project is licensed under the MIT License.